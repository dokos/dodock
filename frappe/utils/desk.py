import frappe


def check_app_permission():
	"""Check if user has permission to access the app (for showing the app on app screen)"""
	if len(frappe.get_installed_apps()) == 1:
		return True

	return False
