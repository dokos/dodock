"""Basic telemetry for improving apps.

WARNING: Everything in this file should be treated "internal" and is subjected to change or get
removed without any warning.
"""

# from contextlib import suppress

# import frappe
# from frappe.utils import getdate
# from frappe.utils.caching import site_cache

# from posthog import Posthog  # isort: skip

POSTHOG_PROJECT_FIELD = "posthog_project_id"
POSTHOG_HOST_FIELD = "posthog_host"


def add_bootinfo(bootinfo):
	return  # @dokos: no telemetry


def site_age():
	return  # @dokos: no telemetry


def init_telemetry():
	return  # @dokos: no telemetry


def capture(event, app, **kwargs):
	return  # @dokos: no telemetry


def capture_doc(doc, action):
	return  # @dokos: no telemetry
