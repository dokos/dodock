# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# License: MIT. See LICENSE
import frappe
from frappe import _

field_map = {
	"Contact": [
		"first_name",
		"last_name",
		"address",
		"phone",
		"mobile_no",
		"email_id",
		"is_primary_contact",
	],
	"Address": [
		"address_line1",
		"address_line2",
		"city",
		"state",
		"pincode",
		"country",
		"is_primary_address",
	],
}


def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		"{reference_doctype_label}:Link/{reference_doctype}".format(
			reference_doctype_label=_("{0} name").format(_(filters.get("reference_doctype"))),
			reference_doctype=filters.get("reference_doctype"),
		),
		_("Address Line 1"),
		_("Address Line 2"),
		_("City"),
		_("State"),
		_("Postal Code"),
		_("Country"),
		"{}:Check".format(_("Is Primary Address")),
		_("First Name"),
		_("Last Name"),
		_("Address"),
		_("Phone"),
		_("Email Id"),
		"{}:Check".format(_("Is Primary Contact")),
	]


def get_data(filters):
	reference_doctype = filters.get("reference_doctype")
	reference_name = filters.get("reference_name")

	return get_reference_addresses_and_contact(reference_doctype, reference_name)


def get_reference_addresses_and_contact(reference_doctype, reference_name):
	data = []
	filters = None
	reference_details = frappe._dict()

	if not reference_doctype:
		return []

	if reference_name:
		filters = {"name": reference_name}

	reference_list = [
		d[0] for d in frappe.get_list(reference_doctype, filters=filters, fields=["name"], as_list=True)
	]

	for d in reference_list:
		reference_details.setdefault(d, frappe._dict())
	reference_details = get_reference_details(reference_doctype, "Address", reference_list, reference_details)
	reference_details = get_reference_details(reference_doctype, "Contact", reference_list, reference_details)

	for reference_name, details in reference_details.items():
		if not details:
			continue

		addresses = details.get("address", [])
		contacts = details.get("contact", [])
		if not any([addresses, contacts]):
			result = [reference_name]
			result.extend(add_blank_columns_for("Address"))
			result.extend(add_blank_columns_for("Contact"))
			data.append(result)
		else:
			addresses = list(map(list, addresses))
			contacts = list(map(list, contacts))

			max_length = max(len(addresses), len(contacts))
			for idx in range(0, max_length):
				result = [reference_name]

				result.extend(addresses[idx] if idx < len(addresses) else add_blank_columns_for("Address"))
				result.extend(contacts[idx] if idx < len(contacts) else add_blank_columns_for("Contact"))

				data.append(result)

	return data


def get_reference_details(reference_doctype, doctype, reference_list, reference_details):
	filters = (
		[
			["Dynamic Link", "link_doctype", "=", reference_doctype],
			["Dynamic Link", "link_name", "in", reference_list],
		]
		if reference_doctype != doctype
		else [[reference_doctype, "name", "in", reference_list]]
	)
	fields = ["`tabDynamic Link`.link_name", "name"] + field_map.get(doctype, [])

	records = frappe.get_list(doctype, filters=filters, fields=fields, as_list=True)

	for d in records:
		details = reference_details.get(d[0], reference_details.get(d[1], {}))
		details.setdefault(frappe.scrub(doctype), []).append(d[2:])

	return reference_details


def add_blank_columns_for(doctype):
	return ["" for field in field_map.get(doctype, [])]
