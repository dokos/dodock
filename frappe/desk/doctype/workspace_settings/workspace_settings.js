// Copyright (c) 2024, Frappe Technologies and contributors
// For license information, please see license.txt

frappe.ui.form.on("Workspace Settings", {
	setup(frm) {
		frm.hide_full_form_button = true;
		frm.docfields = [];
		frm.workspace_map = {};
		let workspace_visibilty = JSON.parse(frm.doc.workspace_visibility_json || "{}");

		// build fields from workspaces
		let cnt = 0,
			column_added = false;

		const sort_key = (ws) => [ws.is_standard, __(ws.title)].join("/"); // @dokos
		const allowed_workspaces = frappe.boot.allowed_workspaces // @dokos
			.slice() // @dokos
			.sort((a, b) => sort_key(a).localeCompare(sort_key(b))); // @dokos
		for (let page of allowed_workspaces) { // @dokos
			if (page.public) {
				frm.workspace_map[page.name] = page;
				cnt++;
				frm.docfields.push({
					fieldtype: "Check",
					fieldname: page.name,
					hidden: !frappe.boot.app_data_map[frappe.current_app].workspaces.includes(
						page.name // @dokos
					),
					label: page.label + (page.parent_page ? ` (${__(page.parent_page)})` : ""), // @dokos
					initial_value: workspace_visibilty[page.name] !== 0, // not set is also visible
				});
			}
		}

		frappe.temp = frm;
	},
	validate(frm) {
		frm.doc.workspace_visibility_json = JSON.stringify(frm.dialog.get_values());
		frm.doc.workspace_setup_completed = 1;
	},
	after_save(frm) {
		// reload page to show latest sidebar
		frappe.app.sidebar.reload();
	},
});
