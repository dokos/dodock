# Copyright (c) 2021, Frappe Technologies and Contributors
# License: MIT. See LICENSE
from frappe.tests import IntegrationTestCase, UnitTestCase


class UnitTestDashboardChartSource(UnitTestCase):
	"""
	Unit tests for DashboardChartSource.
	Use this class for testing individual functions and methods.
	"""

	pass


class TestDashboardChartSource(IntegrationTestCase):
	pass
