import frappe


def execute():
	"""Handle new is_public Checkbox for Summary Card"""

	if not frappe.db.has_column("Summary Card", "is_public"):
		return

	docs = frappe.get_all(
		"Summary Card",
		fields=["name", "owner"],
		filters={"is_standard": False},
	)
	frappe.reload_doc("desk", "doctype", "summary_card")

	print("* Setting is_public for Summary Cards")
	for doc in docs:
		if "@" in doc.name:
			continue  # Assume is private

		if not frappe.db.exists(
			"Has Role",
			{
				"parent": doc.owner,
				"role": "System Manager",
			},
		):
			continue

		print(f"  - {doc.name}")
		frappe.db.set_value(
			"Summary Card",
			doc.name,
			"is_public",
			True,
			update_modified=False,
		)
