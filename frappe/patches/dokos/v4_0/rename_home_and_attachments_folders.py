import frappe
from frappe import _

def execute():
	if home_folder := frappe.db.exists("File", {"is_folder": 1, "is_home_folder": 1, "file_name": _("Home")}):
		if home_folder != "Home":
			try:
				frappe.rename_doc("File", home_folder, "Home", force=True)
			except Exception:
				pass

	if attachments_folder := frappe.db.exists("File", {"is_folder": 1, "is_attachments_folder": 1, "file_name": _("Attachments")}):
		if attachments_folder != "Attachments":
			try:
				frappe.rename_doc("File", attachments_folder, "Attachments", force=True)
			except Exception:
				pass