# Copyright (c) 2021, Frappe Technologies and Contributors
# License: MIT. See LICENSE

import frappe
from frappe.integrations.doctype.incoming_webhook_url.incoming_webhook_url import (
	DiscordParameters,
	GoogleChatParameters,
	IncomingWebhookURL,
	MattermostParameters,
	RocketChatParameters,
	SlackParameters,
)
from frappe.tests import IntegrationTestCase, UnitTestCase
from frappe.utils.data import get_url


class UnitTestIncomingWebhookUrl(UnitTestCase):
	def test_get_data(self):
		params: IncomingWebhookURL = frappe.new_doc("Incoming Webhook URL")  # type: ignore
		args = {
			"params": params,
			"message": "Test",
			"reference_doctype": "User",
			"reference_name": "Administrator",
		}

		TEST_SITE = get_url("")

		self.assertDictEqual(
			SlackParameters(**args).get_data(),
			{
				"attachments": [
					{
						"actions": [
							{
								"style": "primary",
								"text": "Go to the document",
								"type": "button",
								"url": f"{TEST_SITE}/app/user/Administrator",
							}
						],
						"fallback": f"See the document at {TEST_SITE}/app/user/Administrator",
					}
				],
				"text": "Test",
			},
		)

		self.assertDictEqual(
			RocketChatParameters(**args).get_data(),
			{
				"attachments": [
					{
						"title": "Document link",
						"title_link": f"{TEST_SITE}/app/user/Administrator",
					}
				],
				"text": "Test",
			},
		)

		self.assertDictEqual(
			GoogleChatParameters(**args).get_data(),
			{"text": f"Test\n<{TEST_SITE}/app/user/Administrator|Document link>"},
		)

		self.assertDictEqual(
			MattermostParameters(**args).get_data(),
			{"text": f"Test\n<{TEST_SITE}/app/user/Administrator|Document link>"},
		)

		self.assertDictEqual(
			DiscordParameters(**args).get_data(),
			{
				"avatar_url": f"{TEST_SITE}/assets/frappe/images/dokos-logo-round.png",
				"content": "Test",
				"embeds": [{"title": "Document link", "url": f"{TEST_SITE}/app/user/Administrator"}],
				"username": "Dodock",
			},
		)


class TestIncomingWebhookURL(IntegrationTestCase):
	pass
