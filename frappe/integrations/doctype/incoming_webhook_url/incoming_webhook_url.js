// Copyright (c) 2018, Frappe Technologies and contributors
// For license information, please see license.txt

frappe.ui.form.on("Incoming Webhook URL", {
	refresh(frm) {
		if (!frm.is_new() && frappe.user_roles.includes("System Manager")) {
			frm.add_custom_button(__("Send"), function () {
				frm.trigger("show_send_dialog");
			});
		}
	},

	show_send_dialog(frm) {
		var d = new frappe.ui.Dialog({
			fields: [
				{
					fieldtype: "Text",
					label: __("Message"),
					fieldname: "message",
				},
				{
					fieldtype: "Link",
					options: "DocType",
					label: __("Reference Doctype"),
					fieldname: "reference_doctype",
				},
				{
					fieldtype: "Dynamic Link",
					options: "reference_doctype",
					label: __("Reference Name"),
					fieldname: "reference_name",
				},
			],
		});
		d.set_value("message", "Test");
		d.set_value("reference_doctype", frm.doc.doctype);
		d.set_value("reference_name", frm.doc.name);
		d.set_primary_action(__("Send"), async () => {
			const values = d.get_values();
			if (values) {
				d.hide();
				frm.call("send_test_message", values);
			}
		});
		d.show();
	},
});
