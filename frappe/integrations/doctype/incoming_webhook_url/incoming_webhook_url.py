# Copyright (c) 2018, Frappe Technologies and contributors
# For license information, please see license.txt


import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import get_url_to_form


class IncomingWebhookParametersBase:
	def __init__(
		self,
		params: "IncomingWebhookURL",
		message: str,
		reference_doctype: str,
		reference_name: str,
	) -> None:
		self.params = params
		self.message = message
		self.reference_doctype = reference_doctype
		self.reference_name = reference_name

	@property
	def doc_url(self):
		return get_url_to_form(self.reference_doctype, self.reference_name)

	def get_data(self):
		if self.params.add_document_link:
			return {"text": f"{self.message}\n<{self.doc_url}|{_('Document link')}>"}
		else:
			return {"text": self.message}

	def get_error_messages(self):
		return {}


class SlackParameters(IncomingWebhookParametersBase):
	def get_data(self):
		data: dict = {"text": self.message}
		if self.params.add_document_link:
			data.update(
				{
					"attachments": [
						{
							"fallback": _("See the document at {0}").format(self.doc_url),
							"actions": [
								{
									"type": "button",
									"text": _("Go to the document"),
									"url": self.doc_url,
									"style": "primary",
								}
							],
						}
					]
				}
			)
		return data

	def get_error_messages(self):
		return {
			400: "400: Invalid Payload or User not found",
			403: "403: Action Prohibited",
			404: "404: Channel not found",
			410: "410: The Channel is Archived",
			500: "500: Rollup Error, Slack seems to be down",
		}


class RocketChatParameters(IncomingWebhookParametersBase):
	def get_data(self):
		data: dict = {"text": self.message}

		if self.params.add_document_link:
			data["attachments"] = [{"title": _("Document link"), "title_link": self.doc_url}]

		return data


class GoogleChatParameters(IncomingWebhookParametersBase):
	pass


class MattermostParameters(IncomingWebhookParametersBase):
	pass


class DiscordParameters(IncomingWebhookParametersBase):
	def get_data(self):
		from frappe.utils.data import get_url

		app_logo: str = (
			frappe.get_website_settings("app_logo")  # type: ignore
			or "/assets/frappe/images/dokos-logo-round.png"
		)
		data: dict = {
			"content": self.message[:2000],
			"username": frappe.get_website_settings("app_name"),
			"avatar_url": get_url(app_logo),
		}

		if self.params.add_document_link:
			data["embeds"] = [{"title": _("Document link"), "url": self.doc_url}]

		return data


class IncomingWebhookURL(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		add_document_link: DF.Check
		service: DF.Literal["Google Chat", "Mattermost", "Rocket.Chat", "Slack", "Discord"]
		webhook_name: DF.Data
		webhook_url: DF.SmallText
	# end: auto-generated types

	def get_service_class(self) -> "type[IncomingWebhookParametersBase] | None":
		try:
			class_key = "".join(e for e in self.service if e.isalnum())
			service = globals()[f"{class_key}Parameters"]
			return service  # type: ignore
		except Exception:
			frappe.log_error()
			return

	def send(self, message: str, reference_doctype: str, reference_name: str):
		import requests

		service_class = self.get_service_class()
		if not service_class:
			return

		service = service_class(self, message, reference_doctype, reference_name)
		data = service.get_data()

		r = requests.post(self.webhook_url, json=data)

		if not r.ok:
			err_message = ""

			if callable(getattr(service, "get_error_messages", None)):
				err_message = service.get_error_messages().get(r.status_code, r.status_code)

			if not err_message:
				err_message = f"{r.status_code}: {r.text}"

			frappe.log_error(err_message, _("{0} Webhook Error").format(self.service))
			return "error"

		return "success"

	@frappe.whitelist()
	def send_test_message(self, message: str, reference_doctype: str, reference_name: str):
		frappe.only_for("System Manager")
		self.send(message, reference_doctype, reference_name)
