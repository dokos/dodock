import json

from pathlib import Path

import frappe

def extract(fileobj, *args, **kwargs):
	file_path = Path(fileobj.name)
	doctype_path = file_path.parent
	doctype_name = doctype_path.parts[-1]
	json_path = Path(doctype_path, f"{doctype_name}.json")

	if not json_path.exists():
		return

	with open(json_path) as f:
		data = json.loads(f.read())
		doctype_name = data.get("name")

	for line in fileobj.readlines():
		messages = (doctype_name, frappe.safe_decode(line))
		yield None, "pgettext", messages, [f"Contextual translation for {frappe.safe_decode(line)} and doctype {doctype_name}"]