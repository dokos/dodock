import json


def extract(fileobj, *args, **kwargs):
	"""
	Extract messages from DocType JSON files. To be used to babel extractor
	:param fileobj: the file-like object the messages should be extracted from
	:rtype: `iterator`
	"""
	data = json.load(fileobj)

	if isinstance(data, list):
		return

	if data.get("doctype") != "Web Form":
		return

	webform_name = data.get("name")

	yield None, "_", data.get("title"), [f"Title of the {webform_name} web form"]

	if data.get("introduction_text"):
		yield None, "_", data.get("introduction_text"), [f"Introduction of the {webform_name} web form"]

	if data.get("success_title"):
		yield None, "_", data.get("success_title"), [f"Success title of the {webform_name} web form"]

	if data.get("success_message"):
		yield None, "_", data.get("success_message"), [f"Success message of the {webform_name} web form"]

	if data.get("list_title"):
		yield None, "_", data.get("list_title"), [f"List title of the {webform_name} web form"]

	if data.get("button_label"):
		yield None, "_", data.get("button_label"), [f"Button label of the {webform_name} web form"]

	if data.get("meta_title"):
		yield None, "_", data.get("meta_title"), [f"Meta label of the {webform_name} web form"]

	if data.get("meta_description"):
		yield None, "_", data.get("meta_description"), [f"Meta description of the {webform_name} web form"]

	yield from (
		(None, "_", field.get("label"), [f"Label of a field in the {webform_name} web form"])
		for field in data.get("web_form_fields", []) if field.get("label")
	)