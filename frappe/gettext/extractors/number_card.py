import json


def extract(fileobj, *args, **kwargs):
	"""
	Extract messages from Module Onboarding JSON files.

	:param fileobj: the file-like object the messages should be extracted from
	:rtype: `iterator`
	"""
	data = json.load(fileobj)

	if isinstance(data, list):
		return

	if data.get("doctype") != "Number Card":
		return

	if number_card := data.get("name"):
		yield None, "_", number_card, [f"Name of the Number Card'{number_card}'"]
