import json


def extract(fileobj, *args, **kwargs):
	"""
	Extract messages from DocType JSON files. To be used to babel extractor
	:param fileobj: the file-like object the messages should be extracted from
	:rtype: `iterator`
	"""
	data = json.load(fileobj)

	if isinstance(data, list):
		return

	if data.get("doctype") != "Web Template":
		return

	web_template_name = data.get("name")

	yield None, "_", data.get("name"), [f"Name of a web template"]

	yield from (
		(None, "_", field.get("label"), [f"Label of a field in the {web_template_name} web template"])
		for field in data.get("fields", []) if field.get("label")
	)