import json


def extract(fileobj, *args, **kwargs):
	"""
	Extract messages from Module Onboarding JSON files.

	:param fileobj: the file-like object the messages should be extracted from
	:rtype: `iterator`
	"""
	data = json.load(fileobj)

	if isinstance(data, list):
		return

	if data.get("doctype") != "Dashboard Chart":
		return

	if dashboard_chart := data.get("name"):
		yield None, "_", dashboard_chart, [f"Name of the Dashboard Chart'{dashboard_chart}'"]
