frappe.listview_settings['Report'] = {
	formatters: {
		report_name(value) {
			return __(value)
		}
	},
	button: {
		show(doc) {
			return doc.name;
		},
		get_label(doc) {
			return __("Show Report");
		},
		get_description(doc) {
			return __(doc.name);
		},
		action(doc) {
			switch (doc.report_type) {
				case "Report Builder":
					frappe.set_route("List", doc.ref_doctype, "Report", doc.name);
					break;
				case "Query Report":
					frappe.set_route("query-report", doc.name);
					break;
				case "Script Report":
					frappe.set_route("query-report", doc.name);
					break;
				case "Custom Report":
					frappe.set_route("query-report", doc.name);
					break;
			}
		},
	}
}