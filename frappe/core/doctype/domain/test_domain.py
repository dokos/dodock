# Copyright (c) 2021, Frappe Technologies and Contributors
# License: MIT. See LICENSE

import frappe
from frappe.core.doctype.domain_settings.domain_settings import get_active_modules, update_domains
from frappe.core.page.permission_manager.permission_manager import get_roles_and_doctypes
from frappe.tests import IntegrationTestCase, UnitTestCase


class UnitTestDomain(UnitTestCase):
	"""
	Unit tests for Domain.
	Use this class for testing individual functions and methods.
	"""

	pass


class TestDomain(IntegrationTestCase):
	def setUp(self):
		domain_settings = frappe.get_single("Domain Settings")
		domain_settings.active_domains = []
		domain_settings.save()

	def test_domain_migration(self):
		frappe.db.delete("Domain")
		update_domains()

		domains = frappe.get_all("Domain", pluck="name")
		self.assertIn("Social", domains)

		self.assertTrue(get_active_modules())

	def test_new_domain(self):
		with self.patch_hooks(
			{
				"domains": {"TestDomain": ["frappe.core.doctype.domain.test_domain.get_test_domain_data"],},
			}
		):
			data = frappe.get_domain_data("TestDomain")
			self.assertIn("Event", data["restricted_fields"])
			self.assertIn("google_calendar_event_id", data["restricted_fields"]["Event"])
			self.assertListEqual(['Social', 'Website'], data["modules"])
			self.assertIn("Website Manager", data["restricted_roles"])


	def test_domain_activation_deactivation(self):
		frappe.db.delete("Domain")
		with self.patch_hooks(
			{
				"domains": {"TestDomain": ["frappe.core.doctype.domain.test_domain.get_test_domain_data"],},
			}
		):
			data = frappe.get_domain_data("TestDomain")
			domain_settings = frappe.get_single("Domain Settings")
			self.assertListEqual([], domain_settings.active_domains)
			update_domains()
			domain_settings = frappe.get_single("Domain Settings")
			active_domains = [x.domain for x in domain_settings.active_domains]
			self.assertListEqual(["TestDomain"], active_domains)

			for module in data["modules"]:
				self.assertEqual("TestDomain", frappe.db.get_value("Module Def", module, "restrict_to_domain"))

			for role in data["restricted_roles"]:
				self.assertEqual("TestDomain", frappe.db.get_value("Role", role, "restrict_to_domain"))
				self.assertIn(module, get_active_modules())

			for dt in data["restricted_fields"]:
				meta = frappe.get_meta(dt)
				for field in data["restricted_fields"][dt]:
					self.assertEqual(meta.get_field(field).hidden, 0)

			domain_settings = frappe.get_single("Domain Settings")
			domain_settings.active_domains = []
			domain_settings.save()

			for module in data["modules"]:
				self.assertNotIn(module, get_active_modules())

			for role in data["restricted_roles"]:
				self.assertNotIn(role, get_roles_and_doctypes()["roles"])

			for dt in data["restricted_fields"]:
				meta = frappe.get_meta(dt)
				for field in data["restricted_fields"][dt]:
					self.assertEqual(meta.get_field(field).hidden, 1)



def get_test_domain_data(domain):
	from frappe.domains import get_data
	return get_data(domain=domain, path=frappe.get_app_path("frappe", "core", "doctype", "domain", "test_domain.yaml"))
