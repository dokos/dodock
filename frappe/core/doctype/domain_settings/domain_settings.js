// Copyright (c) 2017, Frappe Technologies and contributors
// For license information, please see license.txt

frappe.ui.form.on("Domain Settings", {
	before_load(frm) {
		frm.trigger("refresh_selector");
	},

	after_save(frm) {
		if (frm.domains_multicheck) {
			frm.fields_dict.domains_html.$wrapper.empty()
			frm.domains_multicheck = false;
			frm.trigger("refresh_selector");
		}

		frappe.ui.toolbar.clear_cache();
	},

	refresh_selector(frm) {
		if (!frm.domains_multicheck) {
			frm.domains_multicheck = frappe.ui.form.make_control({
				parent: frm.fields_dict.domains_html.$wrapper,
				df: {
					fieldname: "domains_multicheck",
					fieldtype: "MultiCheck",
					select_all: true,
					get_data: () => {
						let active_domains = (frm.doc.active_domains || []).map(
							(row) => row.domain
						);

						return frappe.boot.all_domains.map((domain) => {
							return {
								label: domain.title ? __(domain.title) : domain.name,
								value: domain.name,
								description: __(domain.title),
								checked: active_domains.includes(domain.name),
							};
						});
					},
					on_change: () => {
						frm.dirty();
					},
				},
				render_input: true,
			});

			let original_func = frm.domains_multicheck.make_checkboxes;
			frm.domains_multicheck.make_checkboxes = () => {
				original_func.call(frm.domains_multicheck);
				frm.domains_multicheck.$wrapper.find(".label-area").click((e) => {
					let domain = $(e.target).data("unit");
					domain && frm.events.show_description(frm, domain);
					e.preventDefault();
				});
			};
			frm.domains_multicheck.refresh();
		}
	},

	show_description(frm, domain) {
		frappe.call({
			method: "get_domain_description",
			doc: frm.doc,
			args: {
				domain: domain
			}
		}).then(r => {
			const description_dialog = new frappe.ui.Dialog({
				title: __("Enable or disable the following features"),
				fields: [
					{
						fieldtype: "HTML",
						options: r.message,
						read_only: true
					}
				]
			});
			description_dialog.show()
		})
	},

	validate(frm) {
		frm.trigger("set_options_in_table");
	},

	set_options_in_table(frm) {
		let selected_options = frm.domains_multicheck.get_value();
		let unselected_options = frm.domains_multicheck.options
			.map((option) => option.value)
			.filter((value) => {
				return !selected_options.includes(value);
			});

		let map = {},
			list = [];
		(frm.doc.active_domains || []).map((row) => {
			map[row.domain] = row.name;
			list.push(row.domain);
		});

		unselected_options.map((option) => {
			if (list.includes(option)) {
				frappe.model.clear_doc("Has Domain", map[option]);
			}
		});

		selected_options.map((option) => {
			if (!list.includes(option)) {
				frappe.model.clear_doc("Has Domain", map[option]);
				let row = frappe.model.add_child(frm.doc, "Has Domain", "active_domains");
				row.domain = option;
			}
		});

		refresh_field("active_domains");
	},
});
