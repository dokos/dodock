# Copyright (c) 2021, Frappe Technologies and contributors
# License: MIT. See LICENSE


import frappe
from frappe import _
from frappe.model.document import Document


class DomainSettings(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.core.doctype.has_domain.has_domain import HasDomain
		from frappe.types import DF

		active_domains: DF.Table[HasDomain]
	# end: auto-generated types

	def set_active_domains(self, domains):
		active_domains = [d.domain for d in self.active_domains]
		added = False
		for d in domains:
			if d not in active_domains:
				self.append("active_domains", dict(domain=d))
				added = True

		if added:
			self.save()

	def before_validate(self):
		self.check_domain_dependencies()

	def check_domain_dependencies(self):
		active_domains = [d.domain for d in self.active_domains]
		for d in active_domains:
			data = frappe.get_domain_data(d)
			if data.get("depends_on_domain"):
				for dependancy in data.get("depends_on_domain"):
					if dependancy not in active_domains and frappe.db.exists("Domain", dependancy):
						self.append("active_domains", dict(domain=dependancy))

	def on_update(self):
		for d in self.active_domains:
			domain = frappe.get_doc("Domain", d.domain)
			domain.setup_domain()

		frappe.clear_cache() # Needed to reload correctly active domains

		self.restrict_roles_and_modules()
		frappe.clear_cache()

	def restrict_roles_and_modules(self):
		"""Disable all restricted roles and set `restrict_to_domain` property in Module Def"""
		active_domains = frappe.get_active_domains()
		all_domains = list(frappe.get_hooks("domains") or {})

		def remove_role(role):
			frappe.db.delete("Has Role", {"role": role})
			frappe.set_value("Role", role, "disabled", 1)

		for domain in all_domains:
			data = frappe.get_domain_data(domain)
			if not frappe.db.get_value("Domain", domain):
				frappe.get_doc(doctype="Domain", domain=domain).insert()
			if "modules" in data:
				for module in data.get("modules"):
					frappe.db.set_value("Module Def", module, "restrict_to_domain", domain)

					for page in frappe.get_all("Page", filters={"module": module}):
						frappe.db.set_value("Page", page.name, "restrict_to_domain", domain)

					for report in frappe.get_all("Report", filters={"module": module}):
						frappe.db.set_value("Report", report.name, "restrict_to_domain", domain)

			if "restricted_roles" in data:
				for role in data["restricted_roles"]:
					if not frappe.db.get_value("Role", role):
						frappe.get_doc(doctype="Role", role_name=role).insert()
					frappe.db.set_value("Role", role, "restrict_to_domain", domain)

					if domain not in active_domains:
						remove_role(role)

			if "custom_fields" in data:
				if domain not in active_domains:
					inactive_domain = frappe.get_doc("Domain", domain)
					inactive_domain.setup_data()
					inactive_domain.remove_custom_field()

			if "restricted_fields" in data:
				if domain not in active_domains:
					inactive_domain = frappe.get_doc("Domain", domain)
					inactive_domain.setup_data()
					inactive_domain.hide_standard_fields()

		frappe.enqueue("frappe.utils.global_search.rebuild_for_doctype", doctype="Page")
		frappe.enqueue("frappe.utils.global_search.rebuild_for_doctype", doctype="Report")

	@frappe.whitelist()
	def get_domain_description(self, domain):
		if not domain:
			return

		domain_data = frappe.get_domain_data(domain)
		return _(domain_data.get("domain", {}).get("description")) or ""


def get_active_domains() -> list:
	"""get the domains set in the Domain Settings as active domain"""

	def _get_active_domains():
		domains = frappe.get_all(
			"Has Domain", filters={"parent": "Domain Settings"}, fields=["domain"], distinct=True
		)

		active_domains = [row.get("domain") for row in domains]
		active_domains.append("")
		return active_domains

	return frappe.cache.get_value("active_domains", _get_active_domains)


def get_active_modules():
	"""get the active modules from Module Def"""

	def _get_active_modules():
		active_modules = []
		active_domains = get_active_domains()
		for m in frappe.get_all("Module Def", fields=["name", "restrict_to_domain"]):
			if (not m.restrict_to_domain) or (m.restrict_to_domain in active_domains):
				active_modules.append(m.name)
		return active_modules

	return frappe.cache.get_value("active_modules", _get_active_modules)


def update_domains():
	all_domains = frappe.get_all("Domain", pluck="name")
	new_domains = set()
	for domain in list(frappe.get_hooks("domains") or {}):
		if domain not in all_domains:
			domain_data = frappe.get_domain_data(domain)
			doc = frappe.new_doc("Domain")
			doc.domain = domain
			doc.title = domain_data.get("title") or domain
			doc.insert()
			new_domains.add(doc.name)


	frappe.get_single("Domain Settings").set_active_domains(new_domains)


def get_domain_data(module):
	try:
		domain_data = frappe.get_hooks("domains")
		if module in domain_data:
			return frappe._dict(frappe.get_attr(frappe.get_hooks("domains")[module][0])(module))
		return frappe._dict()
	except Exception:
		return frappe._dict()
